#ifndef PACOTE_H
#define PACOTE_H

class pacote{
	private:
			char tag, length, value, hash;
	public:
			pacote();
			pacote(char tag);

			char getTag();
			char* getLength();
			char* getValue();
			char* getHash();

			void setTag(char tag);
			void setLength(char length);
			void setValue(char value);
			void setHash(char hash);
};

#endif