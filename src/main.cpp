#include <iostream>
#include <string.h>
#include "array.hpp"
#include "crypto.hpp"
#include "network.hpp"
#include "pacote.hpp"

using namespace std;

int main(){
	int fd = network::connect("45.55.185.4", 3001);
	if (fd<0){
		cout << "nao conectou" <<endl;
		return 1;
	}else cout<< "conectou\n"<<endl;

	byte valor[] = {0xAB, 0xCD, 0x13, 0xA0};
	array::array* cabecalho = array::wrap(4, valor);
	cout<<*cabecalho<<endl;
	array::array* envia = array::create(3);
	array::array* recebe = array::create(3);
	envia->data[0] = 0xCD;
	envia->data[1] = 0;
	envia->data[2] = 0;
	memcpy(envia->data+4, cabecalho->data, 4);
	cout<<*envia<<endl;
	network::write(fd, cabecalho);
	network::write(fd, envia);
	cout<<"Network read: "<<endl;
	recebe = network::read(fd);

	cout<<*recebe<<endl;
	return 0;
}