#include <iostream>
#include "pacote.hpp"

using namespace std;

pacote::pacote(){
	setTag(0xC0);
}

pacote::pacote(char tag){
	setTag(tag);

}

char pacote::getTag(){
	return tag;
}

char* pacote::getLength(){
	return &length;
}

char* pacote::getValue(){
	return &value;
}

char* pacote::getHash(){
	return &hash;
}

void pacote::setTag(char tag){
	this->tag = tag;
}

void pacote::setLength(char length){
	this->length = length;
}

void pacote::setValue(char value){
	this->value = value;
}

void pacote::setHash(char hash){
	this->hash = hash;
}